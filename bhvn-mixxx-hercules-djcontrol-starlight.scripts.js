// Script support for Mixxx -> Hercules DJControl Starlight
// BHVN edition v1.0.3-SNAPSHOT
// Copyright Tom Surace: 2023-2024
// Licensed under the WTFPL - share and enjoy.

// -- Configuration

// Actual wheel ticks-per-revolution? Not documented.
//
// No acceleration but the deadzone makes it a little unpredictable. 
// Experimentation shows it's probably less than 250 but more than 235.
// Hypothesis: it's higher resolution than 240, but drops ticks when
// you go slow so it /feels/ like it's a little lower resolution.

const WHEEL_TICKS_PER_REV = 230;

// It's tiny, so scale up the ticks a bit to get a little more speed.

const TICKS_PER_ROTATION = WHEEL_TICKS_PER_REV * 1.5;

const SCRATCH_ALPHA = 1.0 / 8;
const SCRATCH_BETA = SCRATCH_ALPHA / 32;

/** 
 * When using the jog wheels to beatjump, how many ticks per beatjump?
 *
 * I like 4 jumps per rotation, that's pretty comfortable to control.
 */
const BEATJUMP_TICKS = WHEEL_TICKS_PER_REV / 4;


// Note that timers have a resolution of 20ms so don't get clever

const BEATFLASH_DELAY_MS = 50;
const SYNCFLASH_MS = 55;         // barely strobe the SYNC LED

/**
 * This one's just to be purdy, but if it's short it gets out of the way
 * of the peak lights and makes a happy strobe. Epilepsy warning obvs, might
 * want to disable the blinkyflash entirely if that's an issue though, and
 * avoid dance clubs. Man that sucks. I digress, in any case it takes a couple
 * ms just to send all the midi so 5ms is really "turn it off ASAP".
 */
const BLINKYFLASH_MS = 20; // the shortest timer we can request

// -- Constants

/**
 * A simple led that can be turned on or off
 *
 * @param channel is the 0-indexed midi channel on which we will send.
 *   0 = channel 1, etc.
 * @param ctrl controller ID for LED control byte
 */
const LED = function(channel, ctrl) {
    /**
     * Status byte to send. It's a note-on/off message so...
     */
    this.status = 0x90 + channel;
    this.ctrl = ctrl;
    this.isOn = false;

    /**
     * If set to false, the LED will continue to track state,
     * but not send MIDI on state changes.
     */
    this.isActive = true;
};

/**
 * flush value to MIDI immediately, regardless of isActive
 */
LED.prototype.send = function() {
    var value;
    if (this.isOn) {
        value = 0x7f;  // Pretty much all LEDs on this controller work the same
    }
    else {
        value = 0x00;
    }

    midi.sendShortMsg(this.status, this.ctrl, value);
};

/**
 * Set LED to new value and possibly update control surface. Value
 * is send if isActive === true.
 *
 * @param value boolean value: true for on.
 */
LED.prototype.set = function(value) {
    this.isOn = value;
    if (this.isActive) {
        this.send();
    } 
}

/**
 * Toggles the state of an LED, and returns the new state
 */
LED.prototype.toggle = function() {
    this.isOn = !this.isOn;
    if (this.isActive) {
        this.send();
    }

    return this.isOn;
};

/**
 * Extend LED to be a tempo-based blinky by hooking
 * into the beat_active signal for the indicated
 * deck group
 */
const TempoLED = function(channel, control, group) {
    LED.call(this, channel, control);

    this.group = group;

    /**
     * If counter > 0, is either on or waiting to become active
     */
    this._counter = 0;
};

TempoLED.prototype = Object.create(LED.prototype);

/**
 * Animation timer callback.
 */
TempoLED.prototype.beginBeatActive = function() {
    if (!this.isActive) {  // Don't blink while inactive!
        return;
    }

    // do a "this.send" but with the inverse value while beat_active.
    // The sent value is not stored.

    var value;
    if (!this.isOn) {
        value = 0x7f;
    }
    else {
        value = 0x00;
    }

    midi.sendShortMsg(this.status, this.ctrl, value);
};

TempoLED.prototype.endBeatActive = function() {
    if (!this.isActive) {  // Don't blink while inactive!
        return;
    }

    var value;
    if (this.isOn) {
        value = 0x7f;
    }
    else {
        value = 0x00;
    }
    midi.sendShortMsg(this.status, this.ctrl, value);
};

/**
 * @param channel - zero-indexed midi channel
 */
const StarlightLED = function(channel, group) {
    this.statusByte = 0x90 + channel;
    this.group = group;

    this.level = 0;

    /**
     * is this channel clipping?
     */
    this.isClipping = false;

    /**
     * Should the "beat indicator" be lit somehow?
     */
    this.isBeatActive = false;

    /**
     * Is user currently touching the scratch surface of the deck?
     */
    this.isTouched = false;

    /**
     * Last value sent to the output. Calculated
     * from "level" and "isClipping" etc.
     */
    this.value = 0x0000;
};

StarlightLED.prototype.beginBeatActive = function() {
    this.isBeatActive = true;
    this.calcAndSend();
};

StarlightLED.prototype.endBeatActive = function() {
    this.isBeatActive = false;
    this.calcAndSend();
};

StarlightLED.prototype.beginTouch = function() {
    this.isTouched = true;
    this.calcAndSend();
};

StarlightLED.prototype.endTouch = function() {
    this.isTouched = false;
    this.calcAndSend();
};

/**
 * Calculate a new value based on the current audio level,
 * clip indicator state, etc.
 * 
 * Starlight LED color is an RGB map with
 *   2-bit color for red and blue,
 *   3-bit color for green. Weird right?
 *
 *  RRGGGBB 
 * 
 * @return the new value (does not update this.value)
 */
StarlightLED.prototype.calcAndSend = function() {
    if (this.isClipping) {
        // If we're clipping, ignore other VU data
        value = 0x60;         // aka "Full RED"
    }
    else {
        // Green is 3 bits so we want value scaled from [0,8) then use floor()
        // the other colors can fend for themselves
        //
        // We also want a range beyond that where we go amber. The Mixxx meters
        // go amber about about 7/8 or 88%, so scale past 8 accordingly
        
        value = (this.level * 8.8); // Tweak this to adjust the "amber" trigger
        if (value >= 8) {   // amber
            // Amber: #ffbf00, which truncates (in 2 bits) to 
            value = 0x74;   // Amber: 11 101 00
        }
        else { // value range [0,8), 3 bits!
            // Weight levels: the levels are only below 4 if you cut the
            // bass pretty heavily, on most real world tracks. Scale the green
            // to the top of the range, and the "happy color" to the very
            // bottom. It might be fun if these overlap a little?
            const GREEN_THRESH = 2.4;
            const BOT_THRESH = 5.6;

            let green;
            if (value < GREEN_THRESH) {
                green = 0;
            }
            else {
                // Green is 3 bits, ooh super high res!
                // scale [GREEN_THRESH,8) to [0,8)
                green = (value - GREEN_THRESH) / (8 - GREEN_THRESH) * 8;
            }
            green = Math.floor(green); // truncate to int

            let botVal;
            if (value > BOT_THRESH) {
                botVal = 0;
            }
            else {
                // Red and blue are only 2 bits. 
                // scale [0,BOT_THRESH] to [0,3)
                // and invert to (3,0]
                
                botVal = (value / BOT_THRESH * 3);
                botVal = Math.floor(botVal);
                botVal = 3 - botVal; // invert
            }

            // Green is 3 bits, in the middle
            
            green = (green << 2);

            // Want to fade to purple at 0vu because that's fun
            let blue = Math.floor(botVal);
            let red = Math.floor(botVal / 1.6);

            value = green | (red << 5) | blue; // nn 000 nn
        }

        if (this.isBeatActive) {  // Blink on beat?
            value = value | 0x23; // 01 000 11 Touch of purple
            // value = value | 0x63; // 11 000 11 full purple
            // value = value | 0x7f; // 11 111 11 STROBE
        }
    }

    if (this.isTouched) {
        // Grey when held, with blinkies
        value = value | 0x2d; // RGB: 01 011 01

        // White when held, with just a hint of signal level left
        // value = value | 0x76; // RGB: 10 101 11
    }
    
    if (this.value != value) {
        this.value = value;
        midi.sendShortMsg(this.statusByte, 0x23, value);
    }
};

/**
 * @param level the audio level returned by the VuMeter control, range [0,1].
 *
 * engine.connectControl("[Channel1]","VuMeterL","led.updateLevel");
 */
StarlightLED.prototype.setLevel = function(level) {
    this.level = level;
    this.calcAndSend();
};

StarlightLED.prototype.setIsClipping = function(isClipping) {
    this.isClipping = isClipping;
    this.calcAndSend();
};

/**
 * Common low-level state not tied to a specific deck etc
 */
const GlobalState = function() {
    /**
     * Is the shift button held?
     */
    this.isShifted = false;
};

const globalState = new GlobalState();

/**
 * Abstraction of one deck 
 *
 * @param deckNumber is the deck number: 1 or 2
 */
const Deck = function(deckNumber) {
    // Base channel for most of what happens on this deck, 
    // zero-indexed (for MIDI). Deck 1 = channel 2 = "0x01"
    var channel = deckNumber;

    /**
     * Deck number, for reference?
     */
    this.deckNumber = deckNumber;

    /**
     * The magic "channel" string we send to mixxx for this deck
     */
    this.group = "[Channel" + deckNumber + "]";

    this.syncLed = new TempoLED(channel, 0x05, this.group);
    this.vinylLed = new LED(channel, 0x03);
    this.starlightLed = new StarlightLED(channel);

    /**
     * Offset (in ticks) from when we first touched the wheel.
     */
    this.beatjumpCounter = 0;

    // Timers to manage the beat flash
    
    this._startBeatTimer = 0;
    this._endSyncBeatTimer = 0;
    this._endStarlightBeatTimer = 0;
};

Deck.prototype.init = function() {
    // Wire up the tempo blinkies

    var _this = this;

    engine.makeConnection(this.group, 'beat_active', 
        function(value, group, controlName) { 
            _this.beatActive(value, group, controlName); 
        }
    );

    // When not tempo-strobing, flash the sync led in standard Mixxx style

    var _this = this;

    engine.makeConnection(this.group, 'sync_mode',
        function(value, group, controlName) { 
            _this.syncLed.set(value > 0.5 && value < 2.5); });

    this.vinylLed.set(true); // Vinyl on by default

    engine.makeConnection(this.group, "vu_meter",
        function(value, group, controlName) {
            _this.starlightLed.setLevel(value); } );

    engine.makeConnection(this.group,"peak_indicator",
        function(value, group, controlName) {
            _this.starlightLed.setIsClipping(value); } );

};

Deck.prototype.beatActive = function(value, group, controlName) {
    if (value !== 1) { // if we exited the beat region
        return;        // ignore
    }

    // Flash something on downbeat. That said...
    //
    // We get the notification 50ms before the beat. MIDI, typical 
    // scenario, takes 5-10ms to reach the controller, less with USB if the
    // controller is fast, but not like 50ms. So wait a bit.
    //
    // Note: Starlight is faster than Vestax, less predelay is needed.
    

    var _this = this;

    if (this._startBeatTimer) {
        engine.stopTimer(this._startBeatTimer);
    }

    this._startBeatTimer =
        engine.beginTimer(BEATFLASH_DELAY_MS, function() {
            _this._startBeatTimer = 0;
            _this.syncLed.beginBeatActive();
            _this.starlightLed.beginBeatActive();

            // After a bit, turn the blinky back off

            if (_this._endSyncBeatTimer) {
                engine.stopTimer(_this._endSyncBeatTimer);
            }

            _this._endSyncBeatTimer = 
                engine.beginTimer(SYNCFLASH_MS, function() {
                        _this._endSyncBeatTimer = 0;
                        _this.syncLed.endBeatActive();
                    },
                    true );

            if (_this._endStarlightBeatTimer) {
                engine.stopTimer(_this._endStarlightBeatTimer);
            }

            _this._endStarlightBeatTimer =
                engine.beginTimer(BLINKYFLASH_MS, function() {
                        _this._endStarlightBeatTimer = 0;
                        _this.starlightLed.endBeatActive();
                    },
                    true );
        },
        true );
};

Deck.prototype.handleVinyl = function(channel, control, value, status, group) {
    if (0 === value) { // Ignore button release
        return;
    }

    // Just toggle the LED. The LED is also our "is vinyl enabled" flag.

    this.vinylLed.set(! this.vinylLed.isOn);
};

Deck.prototype.handleJog = function(channel, control, value, status, group) {
    // Value is 7-bit 2's complement. Sign-extend to be 32-bit
    value = value << 25 >> 25;

    // value = value * JOG_CORRECTION;
    engine.setValue(this.group, 'jog', value);    // Pitch bend (or cue)
};

/**
 * Handler for /unshifted/ wheel touch
 */
Deck.prototype.handleWheelTouch = function(channel, control, value, status, group) {
    if (value !== 0)              // If touched
    {         
        this.beatjumpCounter = 0;
        this.starlightLed.beginTouch();
        var isPlaying = engine.getValue(this.group, "play");

        if (this.vinylLed.isOn    // Scratch if Vinyl is active,
            || (! isPlaying))     // or we're stopped (for cueing)
        {
            engine.scratchEnable(
                this.deckNumber, 
                TICKS_PER_ROTATION, 
                33.3333, 
                SCRATCH_ALPHA, 
                SCRATCH_BETA, 
                true);
        }
    }
    else                          // disable on release
    {
        this.starlightLed.endTouch();
        engine.scratchDisable(this.deckNumber);
    }
};

/**
 * Yup, just when you think things can't get more overlapped and confusing, 
 * there's a separate signal for touching the wheel when shifted. :D  
 *
 * It's done well. You don't get a signal if shift is pressed while the wheel
 * is already touched, but both button releases are guaranteed. 
 */
Deck.prototype.handleShiftedWheelTouch = function(channel, control, value, status, group) {

    if (value !== 0)              // If touched
    {         
        this.beatjumpCounter = 0;
        this.starlightLed.beginTouch();
    }
    else                          // disable on release
    {
        this.starlightLed.endTouch();
    }
};

Deck.prototype.handleScratch = function(channel, control, value, status, group) {
    // If the vinyl button is disabled, we don't "do" scratching, so 
    // treat scratch as a "jog". Weird that the dedicated "vinyl" button
    // doesn't do this in firmware...

    if (! engine.isScratching(this.deckNumber)) {
        this.handleJog(channel, control, value, status, group);
        return;      // ** QUICK EXIT **
    }

    // Value is 7-bit 2's complement. Sign-extend to be 32-bit
    value = value << 25 >> 25;
    engine.scratchTick(this.deckNumber, value);    // scratch
};

Deck.prototype.handleShiftedScratch = function(channel, control, value, status, group) {
    // Value is 7-bit 2's complement. Sign-extend to be 32-bit
    value = value << 25 >> 25;

    // When shifted, beatjump using the current beatjump size at 
    // a rate of, well, less than every tick that's too much.
    //
    // TODO: how good is Mixxx's javascript engine at "round?" We 
    // don't need perfect numerical precision, should we use a local
    // rounding function?

    var before = Math.round(this.beatjumpCounter / BEATJUMP_TICKS);
    this.beatjumpCounter += value;
    var after = Math.round(this.beatjumpCounter / BEATJUMP_TICKS);

    while (before < after) {
        ++ before;
        engine.setValue(this.group, 'beatjump_forward', 1);
    }

    while (before > after) {
        -- before;
        engine.setValue(this.group, 'beatjump_backward', 1);
    }
};

/**
 * Shifted sync: match pitch of other deck, or long-touch to clear pitch adjustment
 */
Deck.prototype.handleShiftedSync = function(channel, control, value, status, group) {
    var _this = this;

    if (value !== 0) { // on button down
        if (this.resetPitchTimer) {
            engine.stopTimer(this.resetPitchTimer);
        }

        this.resetPitchTimer = 
            engine.beginTimer(500, function() {
                _this.resetPitchTimer = null;
                engine.setValue(this.group, 'reset_key', 1);
                engine.setValue(this.group, 'reset_key', 0);
            },
            true /* one-shot */ );
    }
    else {                // button release
        // If a timer is still running, it means this was a "short press"

        if (this.resetPitchTimer) {
            engine.stopTimer(this.resetPitchTimer);
            this.resetPitchTimer = null;

            // It's a short-press, sync key with other deck

            engine.setValue(this.group, 'sync_key', 1);
            engine.setValue(this.group, 'sync_key', 0);
        }
    }
};

const DJCStarlight = function() {
    this.deck1 = new Deck(1);
    this.deck2 = new Deck(2);
}

DJCStarlight.prototype.init = function(id) {
    print("DJCStarlight init...");

    // Notes:

    // From the DJuiced "deck 3-4 mapping", this startup sysex
    //   F07E7F060200014E02001B0001000000F7
    // 

    // Request controller name and id:
    //   F0 7E 7F 06 01 F7
    // Of course we can't do anything with the reply...

    engine.setValue("[App]", "num_decks", 2);

    this.deck1.init();
    this.deck2.init();

    /**
     * Button state of CUE MASTER shift button.
     */
    this.isCueMasterEnabled = 0;

    // Request sending of status of all controls (same on all new Herc boards)
    midi.sendShortMsg(0xb0, 0x7f, 0x7f);

    print("DJCStarlight init done");
};

DJCStarlight.prototype.shutdown = function(id) {
    // Lights out

    print("DJCStarlight shut down complete");
};

DJCStarlight.prototype.handleShift = function(channel, control, value, status, group) {
    if (value !== 0) {
        globalState.isShifted = true;
    }
    else {
        globalState.isShifted = false;
    }
};

/**
 * Maybe someday Mixxx will have built-in support for "mix to phones" but
 * right now it's 100% manual. :P
 */
DJCStarlight.prototype.handleCueMaster = function(channel, control, value, status, group) {
    if (value === 0) {
        return; // Ignore release
    }

    // Nothing clever. Just toggle it between 100% mains and
    // "mostly cue" based on button state. 

    // headMix goes from -1 (100% cue) to 1 (100% mains)

    if (this.isCueMasterEnabled) {
        engine.setValue("[Master]", "headMix", -0.8);  // mostly cue
        this.isCueMasterEnabled = false;
    }
    else {
        engine.setValue("[Master]", "headMix", 1);     // 100% mains
        this.isCueMasterEnabled = true;
    }
};


/**
 * DJCStarlight mapping entry point.
 */
var Controller = new DJCStarlight();

